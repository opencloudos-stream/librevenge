%global apiversion 0.0

Summary: A base library for writing document import filters
Name: librevenge
Version: 0.0.5
Release: 4%{?dist}
License: ( LGPL-2.1-or-later OR MPL-2.0 ) AND BSD-3-Clause
URL: http://sourceforge.net/p/libwpd/wiki/librevenge/
Source: http://downloads.sourceforge.net/libwpd/%{name}-%{version}.tar.xz

BuildRequires: gcc-c++ make boost-devel
BuildRequires: pkgconfig(cppunit) pkgconfig(zlib)

%description
librevenge is a base library for writing document import filters.
It has interfaces for text documents, vector graphics, spreadsheets
and presentations.

%package devel
Summary: Development files for %{name}
Requires: %{name} = %{version}-%{release}

%description devel
This package provides libraries and header files for developing
applications that use %{name}.

%package gdb
Summary: gdb pretty printers for %{name}
Requires: gdb
Requires: python3-six
Requires: %{name} = %{version}-%{release}
Supplements: %{name}-debuginfo = %{version}-%{release}

%description gdb
This package contains gdb pretty printers that help with
debugging applications that use %{name}.

%prep
%autosetup

%build
%configure \
    --disable-silent-rules \
    --disable-static \
    --disable-werror \
    --without-doc \
    --enable-pretty-printers

sed -i \
    -e 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' \
    -e 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' \
    libtool
%make_build

%install
%make_install
rm -f %{buildroot}/%{_libdir}/*.la
rm -rf %{buildroot}/%{_docdir}/%{name}

%check
export LD_LIBRARY_PATH=%{buildroot}%{_libdir}${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
%make_build check

%files
%license COPYING.*
%doc README NEWS
%{_libdir}/%{name}-%{apiversion}.so.*
%{_libdir}/%{name}-generators-%{apiversion}.so.*
%{_libdir}/%{name}-stream-%{apiversion}.so.*

%files devel
%doc ChangeLog
%{_includedir}/%{name}-%{apiversion}
%{_libdir}/%{name}-%{apiversion}.so
%{_libdir}/%{name}-generators-%{apiversion}.so
%{_libdir}/%{name}-stream-%{apiversion}.so
%{_libdir}/pkgconfig/%{name}-%{apiversion}.pc
%{_libdir}/pkgconfig/%{name}-generators-%{apiversion}.pc
%{_libdir}/pkgconfig/%{name}-stream-%{apiversion}.pc

%files gdb
%{_datadir}/gdb/auto-load%{_libdir}/%{name}-%{apiversion}-gdb.py*
%{_datadir}/gdb/auto-load%{_libdir}/%{name}-stream-%{apiversion}-gdb.py*
%{_datadir}/%{name}/python

%changelog
* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.0.5-4
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.0.5-3
- Rebuilt for loongarch release

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.0.5-2
- Rebuilt for OpenCloudOS Stream 23.09

* Tue Aug 22 2023 kianli <kianli@tencent.com> - 0.0.5-1
- Upgrade to 0.0.5

* Tue Aug 01 2023 rockerzhu rockerzhu@tencent.com - 0.0.4-4
- Rebuilt for boost 1.82.0

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.0.4-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.0.4-2
- Rebuilt for OpenCloudOS Stream 23

* Sat Nov 26 2022 cunshunxia <cunshunxia@tencent.com> - 0.0.4-1
- initial build
